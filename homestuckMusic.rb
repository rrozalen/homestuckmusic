require 'rubygems'
require 'open-uri'
require 'nokogiri'
require 'restclient'
require 'optparse'

BANDCAMP_URL = 'http://homestuck.bandcamp.com'
OUTPUT = 'output.txt'

# Song list
music = []

# Crawling process
html_object = Nokogiri::HTML(open(BANDCAMP_URL + '/music'))
grid = html_object..css('ol.editable-grid li a')
albums = grid.map { |link| BANDCAMP_URL + link.attribute('href').to_s }

albums.each do |album_url|
  songs = Nokogiri::HTML(open(album_url)).css('td.title-col div.title a')
  album_name = album_url.split('/').last
  song_names = songs.map { |link| album_name + ' - ' + link.content }
  album = {}
  album[:title] = album_name
  album[:songs] = song_names
  music << album
  puts album[:title]
end

# Writing and printing
out = File.open(OUTPUT, 'w')
music.each do |album|
  output_line = ''
  album[:songs].each do |song|
    output_line += song + "\n"
  end
  out.write(output_line)
  puts output_line
end
out.close
